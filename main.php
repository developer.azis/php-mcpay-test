<?php
include 'Question.php';

$question = new Question();

$nums = [3,1,4,2];
echo "Answer first question = ". implode(",",$question->firstQuestion($nums));
echo "<br>";

$nums = [1,2,3,4];
$x = 4;
echo "Answer second question = ". implode(",",$question->secondQuestion($nums,$x));
echo "<br>";

$x = 4;
$word = "souvenir loud four lost";
echo "Answer third question = ".implode(",",$question->thirdQuestion($word,$x));
