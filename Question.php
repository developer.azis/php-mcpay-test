<?php
class Question{

    function firstQuestion($nums){
        $result = [];
        foreach($nums as $value){
            $count = 0;
            foreach($nums as $subtractor){
                $substract = $value - $subtractor;
                if($substract < 0){
                    $count++;
                }
            }
            
            if($count == 0){
                array_push($result, $value);
            }
        }
        return $result;
    }

    public function secondQuestion($nums,$x)
    {
        $result = [];
        foreach ($nums as $num) {
            $count = 0;
            foreach ($nums as $divider) {
                $divide = $num / $divider;
                if($divide == $x){
                    $count++;
                }    
            }
            if($count == 0){
                array_push($result,$num);
            }
            
        }
        return $result;
    }

    public function thirdQuestion($word,$x)
    {
        $result = [];
        $arrayWord = explode(" ",$word);
        foreach ($arrayWord as $str) {
            if(strlen($str) == $x){
                array_push($result,$str);
            }
        }

        return $result;
    }
}
